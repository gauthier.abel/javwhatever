package Tamagotchi;

import java.util.Timer;
import java.util.TimerTask;

public class TimeoutTimer extends TimerTask
{
   private TamaHeart theHeart;
   private Timer     tmr;
   
   
   /**
    * @roseuid 419DEF5E00A4
    */
   public TimeoutTimer(TamaHeart heart) 
   {
      theHeart = heart;
      tmr      = new Timer();
   }
   
   /**
    * @roseuid 4198CB290305
    */
   public void start() 
   {
      tmr.schedule(this, 300000);    // 5 min.
   }
   
   /**
    * @roseuid 4198CB290412
    */
   public void stop() 
   {
      tmr.cancel();
   }
   

   /* (non-Javadoc)
    * @see java.util.TimerTask#run()
    */
   public void run() {
      theHeart.die();
      tmr.cancel();
   }
}
