package Tamagotchi;

import java.util.Timer;
import java.util.TimerTask;

public class FoodTimer extends TimerTask
{
   private TamaHeart       theHeart;
   private TimeoutTimer    countDown;
   private int             autonomyCounter;
   private int             energyFactor;
   private Timer           tmr;
   
   /**
    * @roseuid 419DEF5E0018
    */
   public FoodTimer(TamaHeart heart) 
   {
      countDown          = null;
      theHeart          = heart;
      autonomyCounter   = 1;
      energyFactor      = 0;
      tmr               = new Timer();
      tmr.scheduleAtFixedRate(this, 0, 1000);
   }
   
   /**
    * @param time
    * @roseuid 4198CA91000E
    */
   public void setTimer(int time) 
   {
      autonomyCounter = 20;
   }
   
   /**
    * @roseuid 4198CC1102F5
    */
   public void startEating() 
   {
      stopDigesting();
      energyFactor = 50;
   }
   
   /**
    * @roseuid 4198CC150002
    */
   public void stopEating() 
   {
      energyFactor = 0;
   }
   
   /**
    * @roseuid 419DF716027C
    */
   public void startDigesting() 
   {
      stopEating();
      energyFactor = -1;
      
      if(countDown!=null)
      {
         countDown.stop();
         countDown=null;
      }
   }
   
   /**
    * @roseuid 419DF71C023F
    */
   public void stopDigesting() 
   {
      energyFactor = 0;
   }

   /* (non-Javadoc)
    * @see java.util.TimerTask#run()
    */
   public void run() {
      autonomyCounter += energyFactor;
      
      if(autonomyCounter<600 && 
         theHeart.getStatus()!=theHeart.STAT_EATING )    // energy getting low
         theHeart.cry();
      
      if(autonomyCounter<1)                              // energy used up
      {
         tmr.cancel();                                   // cancel timer
         theHeart.die();
      }
      
      if(autonomyCounter>45000 && 
         countDown == null)                              // energy full
      {
         theHeart.cry();
         stopEating();
         autonomyCounter=45000;
         countDown = new TimeoutTimer(theHeart);
         countDown.start();
      }
      System.out.println(autonomyCounter);
   }
}
