package Tamagotchi;

public class TamaHeart 
{
   private        int            status         = 0;
   public  final  int            STAT_CRY       = 10;
   public  final  int            STAT_DIE       = 20;
   public  final  int            STAT_EATING    = 30;
   public  final  int            STAT_DIGESTING = 40;
   private        FoodTimer      theFoodTimer;
   private        TimeoutTimer   theTimeoutTimer;
   private        Display        theDisplay;
   private        TamaFrame      theFrame;

   
   /**
    * @param display
    * @roseuid 419DF2C101F6
    */
   public TamaHeart() 
   {
      theFrame    = new TamaFrame(this);
      theDisplay  = theFrame.getDisplay();
      theFrame.show();
   }
   
   /**
    * @roseuid 4198CB4B00B5
    */
   public void start() 
   {
      // initialize
      theFoodTimer = new FoodTimer(this);
      theFoodTimer.setTimer(20);

      // remove from table
      removeFromTable();
   }
   
   /**
    * @roseuid 4198CC330163
    */
   public void cry() 
   {
      theDisplay.cry();
   }
   
   /**
    * @roseuid 4198CC3603D4
    */
   public void die() 
   {
      theDisplay.die();
      theFrame.reset();
   }
   
   /**
    * @roseuid 419DE7E2035C
    */
   public void setToTable() 
   {
      theFoodTimer.startEating();
      
      status = STAT_EATING;
      
      theDisplay.eat();      
   }
   
   /**
    * @roseuid 419DE7E80257
    */
   public void removeFromTable() 
   {
      theDisplay.digest();
      theFoodTimer.startDigesting();

      status = STAT_DIGESTING;
   }
   
   /**
    * @roseuid 41A208770826
    */
   public int getStatus()
   {
      return status;
   }
   
   /**
    * @param args
    * @roseuid 41A208770326
    */
   public static void main(String[] args) 
   {
      new TamaHeart();
   }
}
