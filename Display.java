package Tamagotchi;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Display extends JLabel
{
   
   /**
    * @param heart
    * @roseuid 419DFCA80197
    */
   public Display() 
   {
      this.setHorizontalAlignment(CENTER);
   }
   
   /**
    * @roseuid 4198CC620305
    */
   public void digest() 
   {
      this.setIcon(new ImageIcon("tamhappy.gif"));
      this.setText("Digesting");   }
   
   /**
    * @roseuid 4198CC660062
    */
   public void cry() 
   {
      this.setIcon(new ImageIcon("tamcry.gif"));
      this.setText("Crying");
   }
   
   /**
    * @roseuid 4198CC6900B7
    */
   public void die() 
   {
      this.setIcon(new ImageIcon("dieded.gif"));
      this.setText("Dead");
   }
   
   /**
    * @roseuid 419DF5F10221
    */
   public void eat() 
   {
      this.setIcon(new ImageIcon("tameat.gif"));
      this.setText("Eating");
   }
}
