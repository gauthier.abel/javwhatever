package Tamagotchi;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;

public class StartButton extends JButton implements ActionListener
{
   private TamaHeart theHeart;
   
   /**
    * @param frame
    * @roseuid 419DEF5E013B
    */
   public StartButton(TamaHeart heart) 
   {
      theHeart = heart;
      setText("Start");
      this.addActionListener(this);
   }
   
   /**
    * @roseuid 4198CCD400C5
    */
   public void start() 
   {
      theHeart.start();
      setEnabled(false);
   }

   /* (non-Javadoc)
    * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
    */
   public void actionPerformed(ActionEvent e) {
      start();
   }
}
