package Tamagotchi;

import javax.swing.*;
import java.awt.*;

public class TamaFrame extends JFrame
{
   private  TamaHeart      theHeart;
   private  StartButton    theStartButton;
   private  TableButton    theTableButton;
   private  Display        theDisplay;
   
   /**
    * @param heart
    * @roseuid 419DFCA8015B
    */
   public TamaFrame(TamaHeart heart) 
   {
      // initialization
      theHeart = heart;
      this.setDefaultCloseOperation(EXIT_ON_CLOSE);
      this.setSize(250, 250);
      Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
      this.setLocation((screen.width-getWidth())/2, 
                       (screen.height-getHeight())/2);
      BorderLayout layout = new BorderLayout();
      this.setTitle("Tamagotchi");

      // Display
      theDisplay = new Display();
      
      // south panel (navigation)
      theStartButton = new StartButton(heart);
      theTableButton = new TableButton(heart);
      JPanel pnl = new JPanel(new FlowLayout(FlowLayout.CENTER));
      pnl.add(theStartButton);
      pnl.add(theTableButton);
      
      // add components to frame
      this.getContentPane().add(theDisplay, BorderLayout.CENTER);
      this.getContentPane().add(pnl, BorderLayout.SOUTH);
   }
   
   /**
    * @return Display
    * @roseuid 419DFB240261
    */
   public Display getDisplay() 
   {
      return theDisplay;
   }
   
   
   /**
    * @return Display
    * @roseuid 419DFB240758
    * 
    * Resets the GUI. User may start another game.
    */
   public void reset()
   {
      theStartButton.setEnabled(true);
      theTableButton.setSelected(false);
   }
}
