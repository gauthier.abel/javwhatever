package Tamagotchi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JToggleButton;

public class TableButton extends JToggleButton implements ActionListener
{
   private TamaHeart theHeart;
   
   
   public TableButton(TamaHeart heart) 
   {
      theHeart = heart;
      setText("Set to Table");
      this.addActionListener(this);
   }
   
   /**
    * @roseuid 419DE7B60038
    */
   public void setToTable() 
   {
      theHeart.setToTable();
      setText("Remove from Table");
   }
   
   /**
    * @roseuid 419DE7BC00B9
    */
   public void removeFromTable() 
   {
      theHeart.removeFromTable();
      setText("Set to Table");
   }

   /* (non-Javadoc)
    * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
    */
   public void actionPerformed(ActionEvent e) {
      if(isSelected())
         setToTable();
      else
         removeFromTable();
   }
}
